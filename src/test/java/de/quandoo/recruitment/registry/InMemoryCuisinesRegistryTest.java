package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private final InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();


    private void beforeShouldWork() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
    }

    @Test
    public void shouldWork1() {
        beforeShouldWork();
        cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
    }

    @Test
    public void shouldWork1Ext() {
        beforeShouldWork();
        assertEquals(1, cuisinesRegistry.cuisineCustomers(new Cuisine("french")).size());
        assertEquals("1", cuisinesRegistry.cuisineCustomers(new Cuisine("french")).get(0).getUuid());

        assertEquals(1, cuisinesRegistry.cuisineCustomers(new Cuisine("german")).size());
        assertEquals("2", cuisinesRegistry.cuisineCustomers(new Cuisine("german")).get(0).getUuid());

        assertEquals(1, cuisinesRegistry.cuisineCustomers(new Cuisine("italian")).size());
        assertEquals("3", cuisinesRegistry.cuisineCustomers(new Cuisine("italian")).get(0).getUuid());
    }

    @Test
    public void shouldWork2() {
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test
    public void shouldWork2Ext() {
        beforeShouldWork();
        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(null);
        assertNotNull(customerList);
        assertEquals(0, customerList.size());
    }

    @Test
    public void shouldWork3() {
        cuisinesRegistry.customerCuisines(null);
    }

    @Test
    public void shouldWork3Ext() {
        beforeShouldWork();
        List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(null);
        assertNotNull(cuisineList);
        assertEquals(0, cuisineList.size());
    }

    @Ignore
//    @Test(expected = RuntimeException.class)
    public void thisDoesntWorkYet() {
        cuisinesRegistry.topCuisines(1);
    }

    @Test
    public void testEmptyCuisine() {
        List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(new Customer("1"));
        assertEquals( "Expected empty list", 0, cuisineList.size());
    }

    @Test
    public void testEmptyCustomer() {
        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(new Cuisine("whatever"));
        assertEquals( "Expected empty list", 0, customerList.size());
    }

    @Test
    public void testMultipleCuisines() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("italian"));

        List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(new Customer("1"));
        assertEquals( "Wrong list size", 3, cuisineList.stream().distinct().count());
    }

    @Test
    public void testMultipleCustomers() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("french"));

        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
        assertEquals( "Wrong list size", 3, customerList.stream().distinct().count());
    }

    @Test
    public void testRepeatRegister() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));

        List<Cuisine> cuisineList = cuisinesRegistry.customerCuisines(new Customer("1"));
        assertEquals( "Wrong list size", 1, cuisineList.size());
        assertEquals("Wrong cuisine", "french", cuisineList.get(0).getName());

        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
        assertEquals( "Wrong list size", 1, customerList.size());
        assertEquals("Wrong customer", "1", customerList.get(0).getUuid());
    }

    private void beforeTop(){
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("5"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("6"), new Cuisine("french"));
    }

    @Test
    public void topCuisines(){
        beforeTop();

        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(100);
        assertEquals("Wrong list size", 3, topCuisines.size());
        assertEquals("Wrong cuisine", "french", topCuisines.get(0).getName());
        assertEquals("Wrong cuisine", "german", topCuisines.get(1).getName());
        assertEquals("Wrong cuisine", "italian", topCuisines.get(2).getName());
    }

    @Test
    public void topLimitedCuisines(){
        beforeTop();

        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(2);
        assertEquals("Wrong list size", 2, topCuisines.size());
        assertEquals("Wrong cuisine", "french", topCuisines.get(0).getName());
        assertEquals("Wrong cuisine", "german", topCuisines.get(1).getName());
    }

    @Test
    public void emptyTopLimitedCuisines(){
        beforeTop();

        List<Cuisine> topCuisines = cuisinesRegistry.topCuisines(0);
        assertEquals("Invalid list size", 0, topCuisines.size());

        topCuisines = cuisinesRegistry.topCuisines(-1);
        assertEquals("Invalid list size", 0, topCuisines.size());
    }

    @Test
    public void emptyTopCuisines() {
        assertEquals("Expected empty top", 0, cuisinesRegistry.topCuisines(100).size());
    }

}