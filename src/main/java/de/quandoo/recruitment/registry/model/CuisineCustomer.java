package de.quandoo.recruitment.registry.model;

import java.util.Objects;

/**
 * The 1 to 1 cuisine to customer mapping
 */
public class CuisineCustomer {

    private final Customer customer;
    private final Cuisine cuisine;

    public CuisineCustomer(Customer customer, Cuisine cuisine) {
        this.customer = customer;
        this.cuisine = cuisine;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Cuisine getCuisine() {
        return cuisine;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CuisineCustomer that = (CuisineCustomer) o;
        return Objects.equals(customer, that.customer) &&
                Objects.equals(cuisine, that.cuisine);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customer, cuisine);
    }
}
