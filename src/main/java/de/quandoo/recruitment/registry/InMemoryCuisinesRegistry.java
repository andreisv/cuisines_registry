package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.CuisineCustomer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final Map<String, Customer> customers = new ConcurrentHashMap<>();
    private final Map<String, Cuisine> cuisines = new ConcurrentHashMap<>();
    private final Set<CuisineCustomer> cuisineCustomerSet = Collections.synchronizedSet(new HashSet<>());

    private boolean isValid(Customer customer) {
        return customer != null && customer.getUuid() != null && !customer.getUuid().isEmpty();
    }

    private boolean isValid(Cuisine cuisine) {
        return cuisine != null && cuisine.getName() != null && !cuisine.getName().isEmpty();
    }

    @Override
    public void register(final Customer customer, final Cuisine cuisine) {
        if (isValid(customer) && isValid(cuisine)) {
            Optional<Customer> optCustomer = Optional.ofNullable(customers.putIfAbsent(customer.getUuid(), customer));
            Optional<Cuisine> optCuisine = Optional.ofNullable(cuisines.putIfAbsent(cuisine.getName(), cuisine));

            cuisineCustomerSet.add(new CuisineCustomer(optCustomer.orElse(customer), optCuisine.orElse(cuisine)));
        }
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return isValid(cuisine) ?
                cuisineCustomerSet.stream().filter(e -> cuisine.equals(e.getCuisine()))
                        .map(CuisineCustomer::getCustomer).collect(Collectors.toList())
                : Collections.emptyList();
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return isValid(customer) ?
                cuisineCustomerSet.stream().filter(e -> customer.equals(e.getCustomer()))
                        .map(CuisineCustomer::getCuisine).collect(Collectors.toList())
                : Collections.emptyList();
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return n > 0 ?
                cuisineCustomerSet.stream().collect(Collectors.groupingBy(CuisineCustomer::getCuisine, Collectors.counting()))
                        .entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                        .limit(n).map(Map.Entry::getKey).collect(Collectors.toList())
                : Collections.emptyList();
    }
}
